# 鹊桥（内网穿透）

#### 介绍
[使用java基于aio/nio实现的内网穿透工具，致力于帮助开发者内网开发供外部调试使用。](http://blog.jiucheng.org/article/10097)
开放鹊桥平台：[http://letsblog.cn](http://letsblog.cn)
#### 软件架构
![软件架构图](https://images.gitee.com/uploads/images/2021/0430/000911_6bbf760a_120615.png "system.png")

#### 源码打包正式包

```
mvn clean install -Dmaven.test.skip -Denv=release
```

#### 安装教程

1. 准备公网（假设公网ip=10.1.1.22）服务器，安装openjdk1.8+
   下载发行版本（magpiebridge-server-1.0.4.tar.gz）服务端
   解压magpiebridge-server-1.0.4.tar.gz到指定目录
  （tar -zxvf magpiebridge-server-1.0.4.tar.gz -C /usr/local/magpiebridge-server）
   解压后目录及文件结构如下：
   ```
   \_bin
       |_startup.sh
       |_stop.sh
   \_conf
       |_cfg.properties
   \_lib
       |_magpiebridge-common-0.0.3.jar
       |_magpiebridge-server-0.0.3.jar
   \_logs
   ```
   配置服务端端口（供客户端连接使用）、代理端端口（对外服务端口）（vim /usr/local/magpiebridge-server/conf/cfg.properties）
   ```
   # server
   # 服务端IP及端口
   server.ip=0.0.0.0
   server.port=9799
   
   # 根据域名自动分发转向
   server.mappings=0.0.0.0:80

   # 代理端端口映射内网具体端口
   server.d563d057c1bc45f781459faf8bf5b32b.mappings=0.0.0.0:13389/192.168.1.102:3389,0.0.0.0:13306/127.0.0.1:3306,onlyfordev.jiucheng.org/127.0.0.1:8080
   
   # 是否接入管理端，开启后“代理端端口映射内网具体端口”此配置文件无需配置，可登陆管理端配置
   server.mappings.http.opened=N
   # 服务组令牌，连接管理端使用
   server.mappings.http.token=d89f3a35931c386956c1a402a8e09941
   # 管理端地址
   server.mappings.http.url=http://127.0.0.1:9800
   ```
   如上d563d057c1bc45f781459faf8bf5b32b（客户端的密钥）供客户端配置使用，来确认打开具体代理端口
   用户访问公网服务器的13389端口会被转发请求到内网IP（192.168.1.102）3389端口
   同样用户访问公网服务器的13306端口会被转发请求到内网IP（127.0.0.1即客户端允许机器）3306端口
   启动服务端及代理端
   ```
   /usr/local/magpiebridge-server/bin/startup.sh
   ```
   关闭服务端及代理端
   ```
   /usr/local/magpiebridge-server/bin/stop.sh
   ```
2. 准备内网服务器，安装openjdk1.8+
   下载发行版本（magpiebridge-client-1.0.4.tar.gz）客户端
   解压magpiebridge-client-1.0.4.tar.gz到指定目录
  （tar -zxvf magpiebridge-client-1.0.4.tar.gz -C /usr/local/magpiebridge-client）
   解压后目录及文件结构如下：
   ```
   \_bin
       |_startup.sh
       |_stop.sh
   \_conf
       |_cfg.properties
   \_lib
       |_magpiebridge-common-0.0.3.jar
       |_magpiebridge-client-0.0.3.jar
   \_logs
   ```
   配置客户端的服务端IP及端口（vim /usr/local/magpiebridge-client/conf/cfg.properties）
   ```
   # server info
   # 配置公网服务器的服务端IP及端口
   server.ip=10.1.1.22
   server.port=9799

   # 配置（服务端对应客户端的密钥）
   client.key=d563d057c1bc45f781459faf8bf5b32b
   
   # 是否接入管理端
   server.ipport.get.http.opened=N
   # 管理端地址
   server.ipport.get.http.url=http://127.0.0.1:9800
   ```
   启动客户端
   ```
   /usr/local/magpiebridge-client/bin/startup.sh
   ```
   关闭客户端
   ```
   /usr/local/magpiebridge-client/bin/stop.sh
   ```
4. （可选）准备公网服务器，安装openjdk1.8+
   下载发行版本（magpiebridge-manager-1.0.4.tar.gz）管理端
   解压magpiebridge-manager-1.0.4.tar.gz到指定目录
  （tar -zxvf magpiebridge-manager-1.0.4.tar.gz -C /usr/local/magpiebridge-manager）
   解压后目录及文件结构如下：
   ```
   \_bin
       |_startup.sh
       |_stop.sh
   \_conf
       |_cfg.properties
   \_database
       |_db.sql
       |_manager.mv.db
   \_lib
       |_*.jar
   \_logs
       |.
   \_tpl
       |_*.html
   \_webapp
       |_*.*
   ```
   管理端配置文件（vim /usr/local/magpiebridge-manager/conf/cfg.properties）
   ```
    # 管理端IP及端口配置信息
	server.ip=0.0.0.0
	server.port=80
	
	# 扫描包，无需更改
	jiucheng.scanner.package=org.jiucheng.magpiebridge.manager
	
	# 建议部署时随机更改
	jwt.secret=9a96349e2345385785e804e0f4254dee
	
	# 如下数据库配置无需更改，默认使用的是嵌入式数据库
	h2.driverClass=org.h2.Driver
	h2.url=jdbc:h2:#DATABASE#/manager;MODE=MYSQL;DATABASE_TO_LOWER=TRUE;
	h2.username=sa
	h2.password=sa
	h2.minActive=5
	h2.maxActive=20
	h2.maxWait=3000
   ```
   启动管理端
   ```
   /usr/local/magpiebridge-manager/bin/startup.sh
   ```
   关闭管理端
   ```
   /usr/local/magpiebridge-manager/bin/stop.sh
   ```
 管理端登陆账号及密码：
 1、admin@localhost，123456（管理员）
 2、guest@localhost，123456
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 开源许可协议

1. GNU General Public License v2.0