package org.jiucheng.magpiebridge.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

/**
 * 
 * @author jiucheng
 *
 */
public class Cfg {
    
    private static Properties properties = new Properties();
    private static String baseDir;
    
    public static String getServerIp() {
        return properties.getProperty("server.ip");
    }
    
    public static int getServerPort() {
        return Integer.valueOf(properties.getProperty("server.port"));
    }
    
    public static String getProperty(String key) {
    	return properties.getProperty(key);
    }
    
    public static void loadProperties() {
        String file = MessageFormat.format("{0}conf/cfg.properties", baseDir());
        try {
            InputStream is = new FileInputStream(file);
            properties.load(is);
            is.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void buildBaseDir(Class<?> clazz) {
        String path = clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
        if (path.indexOf(".jar") > -1) {
            path = path.substring(0, path.lastIndexOf("/"));
        }
        baseDir = path.substring(0, path.lastIndexOf("/") + 1);
    }
    
    public static String baseDir() {
        return baseDir;
    }
}

