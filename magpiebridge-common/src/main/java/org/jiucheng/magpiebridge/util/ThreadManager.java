package org.jiucheng.magpiebridge.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author jiucheng
 *
 */
public class ThreadManager {
	
	private static final ExecutorService executorService = Executors.newCachedThreadPool();
	
	public static ExecutorService singleton() {
		return executorService;
	}
}
