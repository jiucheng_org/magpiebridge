package org.jiucheng.magpiebridge.manager;

public class MangagerException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 698552268187484916L;
	
	private int code;
	
	public MangagerException(int code, String message) {
		super(message);
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
