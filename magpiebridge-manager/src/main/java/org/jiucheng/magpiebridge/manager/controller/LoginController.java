package org.jiucheng.magpiebridge.manager.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.magpiebridge.manager.dto.OAuth2GithubControlDto;
import org.jiucheng.magpiebridge.manager.out.TplOut;
import org.jiucheng.magpiebridge.manager.service.IOAuth2ControlService;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;

@Controller
public class LoginController {
    
    @Inject("OAuth2ControlService")
    private IOAuth2ControlService OAuth2ControlService;
    
    @RequestMapping(value = "/login", method = RequestMethod.GET, out = TplOut.class)
    public String login(HttpServletRequest request) {
        request.setAttribute("uri", "login");
        request.setAttribute("header-title", "登陆");
        String githubOpened = "F";
        OAuth2GithubControlDto githubControl = OAuth2ControlService.getOauth2GithubControlDto();
        if (githubControl != null && "T".equalsIgnoreCase(githubControl.getOpened())) {
        	githubOpened = "T";
        }
        request.setAttribute("githubOpened", githubOpened);
        return "login";
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.GET, out = TplOut.class)
    public String logout(HttpServletRequest req, HttpServletResponse res) {
        req.setAttribute("uri", "logout");
        req.setAttribute("header-title", "退出");
        Cookie cookie = new Cookie("token", StringUtil.EMPTY);
        cookie.setPath("/");
        cookie.setMaxAge(0);
        res.addCookie(cookie);
        return "logout";
    }
}
