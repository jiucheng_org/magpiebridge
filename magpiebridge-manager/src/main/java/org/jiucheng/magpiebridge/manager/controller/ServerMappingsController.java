package org.jiucheng.magpiebridge.manager.controller;

import java.sql.SQLException;
import java.util.List;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.magpiebridge.manager.dto.ServerIpPortSimpleDto;
import org.jiucheng.magpiebridge.manager.dto.ServerMappingsDto;
import org.jiucheng.magpiebridge.manager.entity.Client;
import org.jiucheng.magpiebridge.manager.entity.ClientMapping;
import org.jiucheng.magpiebridge.manager.entity.Server;
import org.jiucheng.plugin.db.IBaseService;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.Param;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;

@Controller
public class ServerMappingsController {
    
    @Inject
    private IBaseService baseService;
    
    @RequestMapping(value = "/openapi/server/ip/port", method = {RequestMethod.POST, RequestMethod.GET})
    public ServerIpPortSimpleDto serverIpPort(@Param("clientToken") String clientToken) throws SQLException {
        if (StringUtil.isBlank(clientToken)) {
            return ServerIpPortSimpleDto.empty();
        }
        Client client = new Client();
        client.setToken(clientToken);
        List<Client> clients = baseService.list(client);
        if (clients == null || clients.isEmpty()) {
            return ServerIpPortSimpleDto.empty();
        }
        client = clients.get(0);
        Server server = baseService.get(Server.class, client.getServerId());
        if (server == null) {
            return ServerIpPortSimpleDto.empty();
        }
        return new ServerIpPortSimpleDto(server.getIp(), server.getPort());
    }
    
    @RequestMapping(value = "/openapi/server/mappings", method = {RequestMethod.POST, RequestMethod.GET})
    public ServerMappingsDto index(@Param("serverToken") String serverToken, @Param("clientToken") String clientToken) throws SQLException {
        if (StringUtil.isBlank(serverToken) && StringUtil.isBlank(clientToken)) {
            return ServerMappingsDto.empty();
        }
        Server server = new Server();
        server.setToken(serverToken);
        List<Server> list = baseService.list(server);
        if (list == null || list.isEmpty()) {
            return ServerMappingsDto.empty();
        }
        server = list.get(0);
        
        Client client = new Client();
        client.setServerId(server.getId());
        client.setToken(clientToken);
        List<Client> clients = baseService.list(client);
        if (clients == null || clients.isEmpty()) {
            return ServerMappingsDto.empty();
        }
        client = clients.get(0);
        ClientMapping clientMapping = new ClientMapping();
        clientMapping.setClientId(client.getId());
        clientMapping.setServerId(client.getServerId());
        List<ClientMapping> clientMappings = baseService.list(clientMapping);
        if (clientMappings == null || clientMappings.isEmpty()) {
            return ServerMappingsDto.empty();
        }
        return new ServerMappingsDto(server, clientMappings);
    }
}
