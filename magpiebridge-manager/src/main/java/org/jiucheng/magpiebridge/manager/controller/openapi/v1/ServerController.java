package org.jiucheng.magpiebridge.manager.controller.openapi.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.magpiebridge.manager.dto.openapi.v1.ClientReqDto;
import org.jiucheng.magpiebridge.manager.dto.openapi.v1.Openapi;
import org.jiucheng.magpiebridge.manager.entity.Server;
import org.jiucheng.magpiebridge.manager.handler.OpenapiHandler;
import org.jiucheng.magpiebridge.manager.handler.RequestBody;
import org.jiucheng.magpiebridge.manager.out.OpenapiOut;
import org.jiucheng.magpiebridge.manager.service.IServerService;
import org.jiucheng.magpiebridge.manager.util.MD5Util;
import org.jiucheng.magpiebridge.manager.util.ThreadArgs;
import org.jiucheng.plugin.db.IBaseService;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;

@Controller(OpenapiHandler.class)
public class ServerController {
    
    @Inject
    private IBaseService baseService;
    @Inject
    private IServerService serverService;
    
    @RequestMapping(value = "/openapi/v1/server/token/change", method = RequestMethod.POST, out = OpenapiOut.class)
    public Openapi tokenChange(@RequestBody Server reqDto) {
        if (!"T".equalsIgnoreCase(ThreadArgs.getIsSuper())) {
            return Openapi.newInstance(9, "非管理员无法操作");
        }
        if (reqDto == null) 
            return Openapi.newInstance(1, "请求内容不能为空");
        if (reqDto.getId() == null)
            return Openapi.newInstance(2, "编号不能为空");
        Server server = baseService.get(Server.class, reqDto.getId());
        if (server == null)
            return Openapi.newInstance(3, "编号有误");
        server.setToken(MD5Util.getMd5(UUID.randomUUID().toString()));
        baseService.update(server);
        return Openapi.newInstance(server.getToken());
    }
    
    @RequestMapping(value = "/openapi/v1/server/edit", method = RequestMethod.POST, out = OpenapiOut.class)
    public Openapi edit(@RequestBody Server reqDto) {
        if (!"T".equalsIgnoreCase(ThreadArgs.getIsSuper())) {
            return Openapi.newInstance(9, "非管理员无法操作");
        }
        if (reqDto == null) 
            return Openapi.newInstance(1, "请求内容不能为空");
        if (StringUtil.isBlank(reqDto.getName()))
            return Openapi.newInstance(2, "名称不能为空");
        if (StringUtil.isBlank(reqDto.getDomain()))
            return Openapi.newInstance(3, "域名不能为空");
        if (StringUtil.isBlank(reqDto.getIp()))
            return Openapi.newInstance(4, "IP不能为空");
        if (reqDto.getPort() == null)
            return Openapi.newInstance(5, "端口不能为空");
        if (reqDto.getProtocolType() != null
        		&& !(reqDto.getProtocolType() > -1 && reqDto.getProtocolType() < 3))
        	reqDto.setProtocolType(0);
        Server server = baseService.get(Server.class, reqDto.getId());
        if (server == null)
            return Openapi.newInstance(4, "编号有误");
        server.setBlackPorts(Objects.toString(reqDto.getBlackPorts(), StringUtil.EMPTY));
        server.setBlackSubs(Objects.toString(reqDto.getBlackSubs(), StringUtil.EMPTY));
        server.setDomain(reqDto.getDomain());
        server.setIp(reqDto.getIp());
        server.setPort(reqDto.getPort());
        if (reqDto.getProtocolType() != null)
        	server.setProtocolType(reqDto.getProtocolType());
        server.setName(reqDto.getName());
        baseService.update(server);
        return Openapi.newInstance();
    }
    
    @RequestMapping(value = "/openapi/v1/server/del", method = RequestMethod.POST, out = OpenapiOut.class)
    public Openapi del(@RequestBody ClientReqDto reqDto) {
        if (!"T".equalsIgnoreCase(ThreadArgs.getIsSuper())) {
            return Openapi.newInstance(9, "非管理员无法操作");
        }
        if (reqDto == null) 
            return Openapi.newInstance(1, "请求内容不能为空");
        if (StringUtil.isBlank(reqDto.getIds()))
            return Openapi.newInstance(2, "编号不能为空");
        List<Long> idList = new ArrayList<Long>();
        String[] idsStr = reqDto.getIds().split(",");
        for (String idStr : idsStr) {
            if (StringUtil.isNotBlank(idStr) && idStr.matches("[0-9]+")) {
                idList.add(Long.valueOf(idStr));
            }
        }
        serverService.deletetServers(idList);
        return Openapi.newInstance();
    }
    
    @RequestMapping(value = "/openapi/v1/server/add", method = RequestMethod.POST, out = OpenapiOut.class)
    public Openapi add(@RequestBody Server reqDto) {
        if (!"T".equalsIgnoreCase(ThreadArgs.getIsSuper())) {
            return Openapi.newInstance(9, "非管理员无法操作");
        }
        if (reqDto == null) 
            return Openapi.newInstance(1, "请求内容不能为空");
        if (StringUtil.isBlank(reqDto.getName()))
            return Openapi.newInstance(2, "名称不能为空");
        if (StringUtil.isBlank(reqDto.getDomain()))
            return Openapi.newInstance(3, "域名不能为空");
        if (StringUtil.isBlank(reqDto.getIp()))
            return Openapi.newInstance(4, "IP不能为空");
        if (reqDto.getPort() == null)
            return Openapi.newInstance(5, "端口不能为空");
        if (reqDto.getProtocolType() == null
        		|| !(reqDto.getProtocolType() > -1 && reqDto.getProtocolType() < 3))
        	reqDto.setProtocolType(0);
        Server server = new Server();
        server.setBlackPorts(Objects.toString(reqDto.getBlackPorts(), StringUtil.EMPTY));
        server.setBlackSubs(Objects.toString(reqDto.getBlackSubs(), StringUtil.EMPTY));
        server.setDomain(reqDto.getDomain());
        server.setIp(reqDto.getIp());
        server.setPort(reqDto.getPort());
        server.setProtocolType(reqDto.getProtocolType());
        server.setName(reqDto.getName());
        server.setToken(MD5Util.getMd5(UUID.randomUUID().toString()));
        return Openapi.newInstance(baseService.save(server));
    }
}
