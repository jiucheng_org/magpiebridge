package org.jiucheng.magpiebridge.manager.controller.openapi.v1;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.magpiebridge.manager.dto.OAuth2GithubControlDto;
import org.jiucheng.magpiebridge.manager.dto.openapi.v1.Openapi;
import org.jiucheng.magpiebridge.manager.handler.OpenapiHandler;
import org.jiucheng.magpiebridge.manager.handler.RequestBody;
import org.jiucheng.magpiebridge.manager.out.OpenapiOut;
import org.jiucheng.magpiebridge.manager.service.IOAuth2ControlService;
import org.jiucheng.magpiebridge.manager.util.ThreadArgs;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;

/**
 * 
 * @author jiucheng
 *
 */
@Controller(OpenapiHandler.class)
public class SettingsController {

    @Inject("OAuth2ControlService")
    private IOAuth2ControlService OAuth2ControlService;
    
	@RequestMapping(value = "/openapi/v1/oauth2/github/edit", method = RequestMethod.POST, out = OpenapiOut.class)
	public Openapi oauth2Githubedit(@RequestBody OAuth2GithubControlDto githubControlDto) {
        if (!"T".equalsIgnoreCase(ThreadArgs.getIsSuper())) {
            return Openapi.newInstance(9, "非管理员无法操作");
        }
		OAuth2ControlService.updateOAuth2GithubControlDto(githubControlDto);
		return Openapi.newInstance();
	}
}
