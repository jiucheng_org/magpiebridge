package org.jiucheng.magpiebridge.manager.controller.openapi.v1;

import java.util.List;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.magpiebridge.manager.dto.SysUserDto;
import org.jiucheng.magpiebridge.manager.dto.UserPasswordChangeDto;
import org.jiucheng.magpiebridge.manager.dto.openapi.v1.Openapi;
import org.jiucheng.magpiebridge.manager.entity.SysUser;
import org.jiucheng.magpiebridge.manager.entity.SysUserServer;
import org.jiucheng.magpiebridge.manager.handler.OpenapiHandler;
import org.jiucheng.magpiebridge.manager.handler.RequestBody;
import org.jiucheng.magpiebridge.manager.out.OpenapiOut;
import org.jiucheng.magpiebridge.manager.service.ISysUserService;
import org.jiucheng.magpiebridge.manager.util.MD5Util;
import org.jiucheng.magpiebridge.manager.util.ThreadArgs;
import org.jiucheng.plugin.db.IBaseService;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;

@Controller(OpenapiHandler.class)
public class SysUserController {
    
    @Inject
    private IBaseService baseService;
    @Inject
    private ISysUserService sysUserService;
    
    @RequestMapping(value = "/openapi/v1/user/edit", method = RequestMethod.POST, out = OpenapiOut.class)
    public Openapi edit(@RequestBody SysUserDto reqDto) {
        if (!"T".equalsIgnoreCase(ThreadArgs.getIsSuper())) {
            return Openapi.newInstance(9, "非管理员无法操作");
        }
        if (reqDto == null) 
            return Openapi.newInstance(1, "请求内容不能为空");
        if (reqDto.getId() == null)
            return Openapi.newInstance(2, "编号不能为空");
        SysUser sysUser = baseService.get(SysUser.class, reqDto.getId());
        if (sysUser == null)
            return Openapi.newInstance(4, "编号有误");
        if (!("F".equals(reqDto.getInactive()) || "T".equals(reqDto.getInactive()))) {
        	reqDto.setInactive("F");
        }
        if (reqDto.getSubCountLimit() == null) {
        	reqDto.setSubCountLimit(-1L);
        }
        if (reqDto.getPortCountLimit() == null) {
        	reqDto.setPortCountLimit(-1L);
        }
        boolean updated = false;
        if (!reqDto.getInactive().equals(sysUser.getInactive())) {
        	updated = true;
        	sysUser.setInactive(reqDto.getInactive());
        }
        if (StringUtil.isNotBlank(reqDto.getPassword())) {
        	updated = true;
        	sysUser.setPassword(MD5Util.getMd5(reqDto.getPassword()));
        }
        SysUserServer sysUserServer = getSysUserServerByServerId(sysUser.getId());
        if (sysUserServer == null) {
        	sysUserServer = new SysUserServer();
        	sysUserServer.setPortCount(0L);
        	sysUserServer.setPortCountLimit(reqDto.getPortCountLimit());
        	sysUserServer.setServerId(0L);
        	sysUserServer.setSubCount(0L);
        	sysUserServer.setSubCountLimit(reqDto.getSubCountLimit());
        	sysUserServer.setUserId(sysUser.getId());
        	sysUserServer.setVer(0);
        	baseService.save(sysUserServer);
    		sysUserServer = null;
        } else {
        	sysUserServer.setPortCountLimit(reqDto.getPortCountLimit());
        	sysUserServer.setSubCountLimit(reqDto.getSubCountLimit());
        }
        if (updated || sysUserServer != null) {
            sysUserService.updateSysUser(updated ? sysUser : null, sysUserServer);
        }
        return Openapi.newInstance();
    }
    
    private SysUserServer getSysUserServerByServerId(Long userId) {
    	SysUserServer sysUserServerOfSearch = new SysUserServer();
    	sysUserServerOfSearch.setUserId(userId);
    	sysUserServerOfSearch.setServerId(0L);
    	List<SysUserServer> list = baseService.list(sysUserServerOfSearch);
    	if (list != null && !list.isEmpty()) {
    		return list.get(0);
    	}
    	return null;
    }
    
    @RequestMapping(value = "/openapi/v1/user/add", method = RequestMethod.POST, out = OpenapiOut.class)
    public Openapi add(@RequestBody SysUserDto reqDto) {
        if (!"T".equalsIgnoreCase(ThreadArgs.getIsSuper())) {
            return Openapi.newInstance(9, "非管理员无法操作");
        }
        if (reqDto == null) 
            return Openapi.newInstance(1, "请求内容不能为空");
        if (StringUtil.isBlank(reqDto.getEmail()))
            return Openapi.newInstance(2, "邮箱不能为空");
        if (StringUtil.isBlank(reqDto.getPassword()))
            return Openapi.newInstance(3, "密码不能为空");
        if (!("F".equals(reqDto.getInactive()) || "T".equals(reqDto.getInactive()))) {
        	reqDto.setInactive("F");
        }
        if (reqDto.getSubCountLimit() == null) {
        	reqDto.setSubCountLimit(-1L);
        }
        if (reqDto.getPortCountLimit() == null) {
        	reqDto.setPortCountLimit(-1L);
        }
        SysUser sysUser = new SysUser();
        sysUser.setEmail(reqDto.getEmail());
        sysUser.setInactive(reqDto.getInactive());
        sysUser.setIsSuper("F");
        sysUser.setPassword(MD5Util.getMd5(reqDto.getPassword()));
        
        SysUserServer sysUserServer = new SysUserServer();
        sysUserServer.setPortCount(0L);
        sysUserServer.setPortCountLimit(reqDto.getPortCountLimit());
        sysUserServer.setSubCount(0L);
        sysUserServer.setServerId(0L);
        sysUserServer.setSubCountLimit(reqDto.getSubCountLimit());
        sysUserServer.setVer(0);
        return Openapi.newInstance(sysUserService.saveSysUser(sysUser, sysUserServer));
    }
    
    @RequestMapping(value = "/openapi/v1/user/password/change", method = RequestMethod.POST, out = OpenapiOut.class)
    public Openapi tokenChange(@RequestBody UserPasswordChangeDto reqDto) {
        if (reqDto == null) 
            return Openapi.newInstance(1, "请求内容不能为空");
        if (StringUtil.isBlank(reqDto.getNewpwd()))
            return Openapi.newInstance(3, "新密码不能为空");
        SysUser sysUser = baseService.get(SysUser.class, ThreadArgs.getGid());
        if (sysUser == null)
            return Openapi.newInstance(4, "用户不存在");
        if (StringUtil.isNotBlank(sysUser.getPassword())) {
	        if (StringUtil.isBlank(reqDto.getOldpwd()))
	            return Openapi.newInstance(2, "旧密码不能为空");
	        if (!MD5Util.getMd5(reqDto.getOldpwd()).equalsIgnoreCase(sysUser.getPassword()))
	            return Openapi.newInstance(5, "旧密码不正确");
        }
        sysUser.setPassword(MD5Util.getMd5(reqDto.getNewpwd()));
        baseService.update(sysUser);
        return Openapi.newInstance();
    }
}
