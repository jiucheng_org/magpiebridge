package org.jiucheng.magpiebridge.manager.dao.impl;

import javax.sql.DataSource;

import org.jiucheng.ioc.annotation.Component;
import org.jiucheng.ioc.annotation.Impl;
import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.orm.dialect.Dialect;
import org.jiucheng.orm.dialect.impl.MySQLDialect;
import org.jiucheng.plugin.db.BaseDaoImpl;
import org.jiucheng.plugin.db.IBaseDao;

@Component("baseDao")
public class ExtDaoImpl extends BaseDaoImpl implements IBaseDao {
    
    @Inject
    private DataSource dataSource;
    @Impl(MySQLDialect.class)
    private Dialect dialect;

    @Override
    public Dialect getDialect() {
        return dialect;
    }

    @Override
    public DataSource getDataSource() {
        return dataSource;
    }

}
