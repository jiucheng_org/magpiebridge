package org.jiucheng.magpiebridge.manager.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BlackPortsDto implements Comparable<BlackPortsDto>{
    
    private int startPort;
    private int endPort;
    
    public BlackPortsDto(int startPort, int endPort) {
        this.startPort = startPort;
        this.endPort = endPort;
    }

    public int getStartPort() {
        return startPort;
    }

    public void setStartPort(int startPort) {
        this.startPort = startPort;
    }

    public int getEndPort() {
        return endPort;
    }

    public void setEndPort(int endPort) {
        this.endPort = endPort;
    }

    public int compareTo(BlackPortsDto o) {
        if (this.getStartPort() != o.getStartPort()) {
            return this.getStartPort() - o.getStartPort();
        } else {
            return this.getEndPort() - o.getEndPort();
        }
    }
    
    public static void main(String[] args) {
        // 0-10220,12232
        List<BlackPortsDto> list = new ArrayList<BlackPortsDto>();
        list.add(new BlackPortsDto(0, 10221));
        list.add(new BlackPortsDto(12232, 12232));
        Collections.sort(list);
        System.out.println(list);
    }
}
