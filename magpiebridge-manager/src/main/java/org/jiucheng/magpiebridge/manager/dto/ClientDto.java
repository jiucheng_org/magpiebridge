package org.jiucheng.magpiebridge.manager.dto;

import org.jiucheng.magpiebridge.manager.entity.Client;

public class ClientDto extends Client {
    
    private String serverName;
    
    public String getServerName() {
        return serverName;
    }
    
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }
}
