package org.jiucheng.magpiebridge.manager.dto;

/*
{"access_token":"c680a9a0571f02038936ad75ca4eeda8b6a75ad8","token_type":"bearer","scope":""}
850deabf00c1398902a0
{"error":"bad_verification_code","error_description":"The code passed is incorrect or expired.","error_uri":"https://developer.github.com/apps/managing-oauth-apps/troubleshooting-oauth-app-access-token-request-errors/#bad-verification-code"}
850deabf00c1398902a03

 */
public class OAuth2GithubAccessTokenDto {
    private String access_token;
    private String token_type;
    private String scope;

    private String error;
    private String error_description;
    private String error_uri;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public String getError_uri() {
        return error_uri;
    }

    public void setError_uri(String error_uri) {
        this.error_uri = error_uri;
    }
}
