package org.jiucheng.magpiebridge.manager.dto;

import com.alibaba.fastjson.JSON;

/**
 * oauth2 github配置信息
 * 
 * @author jiucheng
 *
 */
public class OAuth2GithubControlDto {
	/**
	 * 是否开启
	 */
	private String opened;
	/**
	 * 应用账号
	 */
	private String clientId;
	/**
	 * 应用密钥
	 */
	private String clientSecret;
	/**
	 * 回调地址
	 */
	private String callback;

	public String getOpened() {
		return opened;
	}

	public void setOpened(String opened) {
		this.opened = opened;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}
	
	public static void main(String[] args) {
		OAuth2GithubControlDto a = new OAuth2GithubControlDto();
		a.setOpened("T");
		a.setClientId("5dd931db6b8e1bc28907");
		a.setClientSecret("2891102689c2b2c0d59bcad010e52cf02af18eaf");
		a.setCallback("http://127.0.0.1:9800/oauth2/callback/github");
		System.out.println(JSON.toJSONString(a));
	}
}
