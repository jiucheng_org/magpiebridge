package org.jiucheng.magpiebridge.manager.dto;

public class OAuth2GithubUserDto {

    // atjiucheng
    private String login;
    private String id;
    // jiucheng.org@gmail.com
    private String email;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
