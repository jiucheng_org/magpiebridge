package org.jiucheng.magpiebridge.manager.dto;

import org.jiucheng.util.StringUtil;

public class ServerIpPortSimpleDto {
    
    private static ServerIpPortSimpleDto empty = new ServerIpPortSimpleDto(null, -1);
    
    public static ServerIpPortSimpleDto empty() {
        return empty;
    }
    
    private String ip;
    private int port;
    
    public ServerIpPortSimpleDto(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }
    
    @Override
    public String toString() {
        if (ip == null) {
            return StringUtil.EMPTY;
        }
        return ip + ":" + port;
    }
}
