package org.jiucheng.magpiebridge.manager.dto;

import java.text.MessageFormat;
import java.util.List;

import org.jiucheng.magpiebridge.manager.entity.ClientMapping;
import org.jiucheng.magpiebridge.manager.entity.Server;
import org.jiucheng.util.StringUtil;

public class ServerMappingsDto {
    private Server server;
    private List<ClientMapping> clientMappings;
    
    private static ServerMappingsDto empty = new ServerMappingsDto(null, null);
    
    public static ServerMappingsDto empty() {
        return empty;
    }

    public ServerMappingsDto(Server server, List<ClientMapping> clientMappings) {
        this.server = server;
        this.clientMappings = clientMappings;
    }
    
    @Override
    public String toString() {
        if (server == null || clientMappings == null || clientMappings.isEmpty()) {
            return StringUtil.EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        for (ClientMapping clientMapping : clientMappings) {
            if (clientMapping != null) {
                if ("HTTP".equalsIgnoreCase(clientMapping.getServerProtocol())) {
                    sb.append(MessageFormat.format("{0}.{1}", clientMapping.getServerIpPort(), server.getDomain())).append("/").append(clientMapping.getLocalIpPort()).append(",");
                } else if ("TCP".equalsIgnoreCase(clientMapping.getServerProtocol())) {
                    sb.append(MessageFormat.format("0.0.0.0:{0}", clientMapping.getServerIpPort())).append("/").append(clientMapping.getLocalIpPort()).append(",");
                }
            }
        }
        return sb.substring(0, sb.length() - 1);
    }
}
