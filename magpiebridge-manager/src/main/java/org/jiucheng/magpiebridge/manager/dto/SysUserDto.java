package org.jiucheng.magpiebridge.manager.dto;

import org.jiucheng.magpiebridge.manager.entity.SysUser;

public class SysUserDto extends SysUser {
	private Long portCountLimit;
	private Long subCountLimit;
	public Long getPortCountLimit() {
		return portCountLimit;
	}
	public void setPortCountLimit(Long portCountLimit) {
		this.portCountLimit = portCountLimit;
	}
	public Long getSubCountLimit() {
		return subCountLimit;
	}
	public void setSubCountLimit(Long subCountLimit) {
		this.subCountLimit = subCountLimit;
	}
}
