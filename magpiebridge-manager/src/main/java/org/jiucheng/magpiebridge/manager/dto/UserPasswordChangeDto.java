package org.jiucheng.magpiebridge.manager.dto;

public class UserPasswordChangeDto {
    private String oldpwd;
    private String newpwd;
    public String getOldpwd() {
        return oldpwd;
    }
    public void setOldpwd(String oldpwd) {
        this.oldpwd = oldpwd;
    }
    public String getNewpwd() {
        return newpwd;
    }
    public void setNewpwd(String newpwd) {
        this.newpwd = newpwd;
    }
}
