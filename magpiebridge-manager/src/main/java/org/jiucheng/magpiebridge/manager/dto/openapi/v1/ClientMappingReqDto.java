package org.jiucheng.magpiebridge.manager.dto.openapi.v1;

public class ClientMappingReqDto {
    private Long id;
    private String protocol;
    private Long clientId;
    private String serverIpPort;
    private String localIpPort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getProtocol() {
        return protocol;
    }
    
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getServerIpPort() {
        return serverIpPort;
    }

    public void setServerIpPort(String serverIpPort) {
        this.serverIpPort = serverIpPort;
    }

    public String getLocalIpPort() {
        return localIpPort;
    }

    public void setLocalIpPort(String localIpPort) {
        this.localIpPort = localIpPort;
    }
}
