package org.jiucheng.magpiebridge.manager.dto.openapi.v1;

public class ClientReqDto {

    private Long id;
    private String name;
    private Long serverId;
    private String ids;
    
    public String getIds() {
        return ids;
    }
    
    public void setIds(String ids) {
        this.ids = ids;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Long getServerId() {
        return serverId;
    }
    
    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }
}
