package org.jiucheng.magpiebridge.manager.dto.openapi.v1;

public class Openapi {
    
    private int sta;
    private String msg;
    private Object obj;

    public int getSta() {
        return sta;
    }

    public void setSta(int sta) {
        this.sta = sta;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
    
    public static Openapi newInstance() {
        Openapi openapi = new Openapi();
        openapi.msg = "成功";
        return openapi;
    }
    
    public static Openapi newInstance(Object obj) {
        Openapi openapi = new Openapi();
        openapi.msg = "成功";
        openapi.obj = obj;
        return openapi;
    }
    
    public static Openapi newInstance(int sta, String msg) {
        Openapi openapi = new Openapi();
        openapi.sta = sta;
        openapi.msg = msg;
        return openapi;
    }
}
