package org.jiucheng.magpiebridge.manager.dto.openapi.v1;

public class ServerAddReqDto {

    private Long id;
    private String name;
    private String serverIpPort;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServerIpPort() {
        return serverIpPort;
    }

    public void setServerIpPort(String serverIpPort) {
        this.serverIpPort = serverIpPort;
    }
}
