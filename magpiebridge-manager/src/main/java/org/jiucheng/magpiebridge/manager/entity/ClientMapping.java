package org.jiucheng.magpiebridge.manager.entity;

/**
 * 隧道管理/映射
 * 
 * @author jiucheng
 *
 */
public class ClientMapping {

    private Long id;
    private Long clientId;
    private Long serverId;
    private String serverProtocol;
    private String serverIpPort;
    private String localIpPort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }
    
    public String getServerProtocol() {
        return serverProtocol;
    }
    
    public void setServerProtocol(String serverProtocol) {
        this.serverProtocol = serverProtocol;
    }

    public String getServerIpPort() {
        return serverIpPort;
    }

    public void setServerIpPort(String serverIpPort) {
        this.serverIpPort = serverIpPort;
    }

    public String getLocalIpPort() {
        return localIpPort;
    }

    public void setLocalIpPort(String localIpPort) {
        this.localIpPort = localIpPort;
    }
}
