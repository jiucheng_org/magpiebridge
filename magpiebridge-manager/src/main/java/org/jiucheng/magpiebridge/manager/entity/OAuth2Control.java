package org.jiucheng.magpiebridge.manager.entity;

import org.jiucheng.orm.annotation.Table;

@Table("oauth2_control")
public class OAuth2Control {
	
	public final class Category {
		/**
		 * github
		 */
		public final static String GITHUB = "GITHUB";
	}

    private Long id;
	private String category;
	private String content;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
