package org.jiucheng.magpiebridge.manager.entity;

/**
 * 服务器资源
 * 
 * @author jiucheng
 *
 */
public class Server {

    private Long id;
    /**
     * 名称
     */
    private String name;
    /**
     * 令牌
     */
    private String token;
    /**
     * 连接IP
     */
    private String ip;
    /**
     * 连接端口
     */
    private Integer port;
    /**
     * 顶级域名，例如jiucheng.org
     */
    private String domain;
    /**
     * 黑名单端口（多个以英文逗号分隔，如：1-9999,10001）
     */
    private String blackPorts;
    /**
     * 黑名单域名前缀（多个以英文逗号分隔,如：www,wap,m）
     */
    private String blackSubs;
    
    /**
     * 支持协议类型
     * 0 所有
     * 1 http
     * 2 tcp
     */
    private Integer protocolType;
    
    public Integer getProtocolType() {
		return protocolType;
	}
    
    public void setProtocolType(Integer protocolType) {
		this.protocolType = protocolType;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getBlackPorts() {
        return blackPorts;
    }

    public void setBlackPorts(String blackPorts) {
        this.blackPorts = blackPorts;
    }

    public String getBlackSubs() {
        return blackSubs;
    }

    public void setBlackSubs(String blackSubs) {
        this.blackSubs = blackSubs;
    }
}
