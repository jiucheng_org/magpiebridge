package org.jiucheng.magpiebridge.manager.entity;

/**
 * 回收的资源
 * 
 * @author jiucheng
 *
 */
public class ServerResource {

    private Long id;
    private Long serverId;
    private String serverProtocol;
    private String serverIpPort;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public String getServerProtocol() {
        return serverProtocol;
    }

    public void setServerProtocol(String serverProtocol) {
        this.serverProtocol = serverProtocol;
    }

    public String getServerIpPort() {
        return serverIpPort;
    }

    public void setServerIpPort(String serverIpPort) {
        this.serverIpPort = serverIpPort;
    }
}
