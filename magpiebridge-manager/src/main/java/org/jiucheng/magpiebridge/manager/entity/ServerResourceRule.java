package org.jiucheng.magpiebridge.manager.entity;

/**
 * 服务器资源的规则
 * 
 * @author jiucheng
 *
 */
public class ServerResourceRule {
    private Long id;
    private Long serverId;
    /**
     * 当前
     */
    private Integer currentPort;
    /**
     * 当前域名前缀
     */
    private String currentSub;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public Integer getCurrentPort() {
        return currentPort;
    }

    public void setCurrentPort(Integer currentPort) {
        this.currentPort = currentPort;
    }

    public String getCurrentSub() {
        return currentSub;
    }

    public void setCurrentSub(String currentSub) {
        this.currentSub = currentSub;
    }
}
