package org.jiucheng.magpiebridge.manager.entity;

/**
 * 用户
 * 
 * @author jiucheng
 *
 */
public class SysUser {

    private Long id;
    private String email;
    private String password;
    private String isSuper;
    private String inactive;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getIsSuper() {
        return isSuper;
    }
    
    public void setIsSuper(String isSuper) {
        this.isSuper = isSuper;
    }
    
    public String getInactive() {
		return inactive;
	}
    
    public void setInactive(String inactive) {
		this.inactive = inactive;
	}
}
