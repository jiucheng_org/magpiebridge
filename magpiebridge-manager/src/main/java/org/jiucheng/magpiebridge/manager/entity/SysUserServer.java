package org.jiucheng.magpiebridge.manager.entity;

import org.jiucheng.orm.annotation.Optimistic;

/**
 * 用户的映射限制资源数及使用数
 * 
 * @author jiucheng
 *
 */
public class SysUserServer {

	private Long id;
	private Long userId;
	private Long serverId;
	private Long portCountLimit;
	private Long subCountLimit;
	private Long portCount;
	private Long subCount;
	@Optimistic
	private Integer ver;
	
	public Long getPortCountLimit() {
		return portCountLimit;
	}

	public void setPortCountLimit(Long portCountLimit) {
		this.portCountLimit = portCountLimit;
	}

	public Long getSubCountLimit() {
		return subCountLimit;
	}

	public void setSubCountLimit(Long subCountLimit) {
		this.subCountLimit = subCountLimit;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Integer getVer() {
		return ver;
	}
	
	public void setVer(Integer ver) {
		this.ver = ver;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getServerId() {
		return serverId;
	}

	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}

	public Long getPortCount() {
		return portCount;
	}

	public void setPortCount(Long portCount) {
		this.portCount = portCount;
	}

	public Long getSubCount() {
		return subCount;
	}

	public void setSubCount(Long subCount) {
		this.subCount = subCount;
	}
}
