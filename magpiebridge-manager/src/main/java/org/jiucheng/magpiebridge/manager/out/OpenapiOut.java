package org.jiucheng.magpiebridge.manager.out;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.jiucheng.web.WebWrapper;
import org.jiucheng.web.handler.Out;
import org.jiucheng.web.util.WebUtil;

import com.alibaba.fastjson.JSON;

public class OpenapiOut implements Out {

    public void invoke(WebWrapper webWrapper, Object rs) {
        if (rs != null) {
            HttpServletResponse response = WebUtil.getResponse();
            response.setCharacterEncoding(WebUtil.getEncoding());
            response.setContentType("application/json;charset=" + WebUtil.getEncoding());
            try {
                PrintWriter out = response.getWriter();
                out.print(JSON.toJSONString(rs));
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
