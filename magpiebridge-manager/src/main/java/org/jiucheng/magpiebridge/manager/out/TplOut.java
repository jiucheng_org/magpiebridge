package org.jiucheng.magpiebridge.manager.out;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jiucheng.magpiebridge.manager.util.Ga;
import org.jiucheng.magpiebridge.manager.util.ThreadArgs;
import org.jiucheng.magpiebridge.util.Cfg;
import org.jiucheng.template.Template;
import org.jiucheng.web.WebWrapper;
import org.jiucheng.web.handler.Out;
import org.jiucheng.web.util.WebUtil;

public class TplOut implements Out {
    
    public TplOut() {
        Template.putClass("ga", Ga.class);
    }
    
    public void invoke(WebWrapper webWrapper, Object rs) {
        if (null == rs)
            return;
        HttpServletResponse response = WebUtil.getResponse();
        response.setCharacterEncoding(WebUtil.getEncoding());
        response.setContentType("text/html;charset=" + WebUtil.getEncoding());
        if (rs instanceof String) {
            HttpServletRequest req = WebUtil.getRequest();
            Enumeration<String> enums = req.getAttributeNames();
            Map<String, Object> context = new HashMap<String, Object>();
            if (enums != null) {
                String k;
                while (enums.hasMoreElements()) {
                    k = enums.nextElement();
                    context.put(k, req.getAttribute(k));
                }
            }
            context.put("gemail", ThreadArgs.getGemail());
            String filePath = MessageFormat.format("{0}tpl/{1}.html", Cfg.baseDir(), (String) rs);
            try {
                PrintWriter out = response.getWriter();
                out.print(Template.get(context, filePath));
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                PrintWriter out = response.getWriter();
                out.print(rs);
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
