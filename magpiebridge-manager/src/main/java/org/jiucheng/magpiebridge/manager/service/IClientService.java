package org.jiucheng.magpiebridge.manager.service;

import java.io.Serializable;
import java.util.List;

import org.jiucheng.aop.Aop;
import org.jiucheng.magpiebridge.manager.dto.ClientDto;
import org.jiucheng.magpiebridge.manager.entity.Client;
import org.jiucheng.magpiebridge.manager.entity.ClientMapping;
import org.jiucheng.magpiebridge.manager.entity.ServerResourceRule;
import org.jiucheng.orm.interceptor.Tx;

public interface IClientService {
    
    List<ClientDto> listClientDto(Long userId);
    
    @Aop(Tx.class)
    void deleteClient(Long id);
    
    @Aop(Tx.class)
    void deleteClients(List<Client> clients);
    
    @Aop(Tx.class)
    void deleteClientMappings(List<ClientMapping> clientMappings);
    
    @Aop(Tx.class)
    Serializable saveClientMappingAndUpdateServerResourceRule(ClientMapping clientMapping, ServerResourceRule serverResourceRule);
}
