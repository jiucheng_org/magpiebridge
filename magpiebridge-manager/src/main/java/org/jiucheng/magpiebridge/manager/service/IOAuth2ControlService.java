package org.jiucheng.magpiebridge.manager.service;

import org.jiucheng.magpiebridge.manager.dto.OAuth2GithubControlDto;

/**
 * 
 * @author jiucheng
 *
 */
public interface IOAuth2ControlService {
	/**
	 * GITHUB配置
	 * 
	 * @return
	 */
	OAuth2GithubControlDto getOauth2GithubControlDto();
	
	/**
	 * 更新GITHUB配置
	 * 
	 * @param githubControlDto
	 */
	void updateOAuth2GithubControlDto(OAuth2GithubControlDto githubControlDto);
}
