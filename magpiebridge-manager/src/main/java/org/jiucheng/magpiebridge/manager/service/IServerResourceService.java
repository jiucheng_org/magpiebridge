package org.jiucheng.magpiebridge.manager.service;

import org.jiucheng.magpiebridge.manager.entity.ServerResource;

public interface IServerResourceService {
    ServerResource lastServerResource(Long serverId, String serverProtocol);
}
