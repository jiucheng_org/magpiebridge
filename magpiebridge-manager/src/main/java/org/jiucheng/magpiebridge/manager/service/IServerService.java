package org.jiucheng.magpiebridge.manager.service;

import java.util.List;

import org.jiucheng.aop.Aop;
import org.jiucheng.orm.interceptor.Tx;

public interface IServerService {
    @Aop(Tx.class)
    void deletetServers(List<Long> ids);
}
