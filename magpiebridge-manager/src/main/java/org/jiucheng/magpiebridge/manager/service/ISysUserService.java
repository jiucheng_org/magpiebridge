package org.jiucheng.magpiebridge.manager.service;

import org.jiucheng.aop.Aop;
import org.jiucheng.magpiebridge.manager.dto.SysUserDto;
import org.jiucheng.magpiebridge.manager.entity.SysUser;
import org.jiucheng.magpiebridge.manager.entity.SysUserServer;
import org.jiucheng.magpiebridge.manager.util.PagePrevNext;
import org.jiucheng.orm.interceptor.Tx;

/**
 * 
 * @author jiucheng
 *
 */
public interface ISysUserService {
	/**
	 * 用戶列表
	 * 
	 * @param currentPage
	 * @param email
	 * @return
	 */
	PagePrevNext<SysUserDto> page(int currentPage, String email);
	
	/**
	 * 新增用戶
	 * 
	 * @param sysUser
	 * @param sysUserServer
	 * @return
	 */
	@Aop(Tx.class)
	Long saveSysUser(SysUser sysUser, SysUserServer sysUserServer);
	
	/**
	 * 更新用戶
	 * 
	 * @param sysUser
	 * @param sysUserServer
	 */
	@Aop(Tx.class)
	void updateSysUser(SysUser sysUser, SysUserServer sysUserServer);
	
	/**
	 * 查找SysUserServer
	 * 
	 * @param userId
	 * @param serverId
	 * @return
	 */
	SysUserServer getSysUserServer(Long userId, Long serverId);
}
