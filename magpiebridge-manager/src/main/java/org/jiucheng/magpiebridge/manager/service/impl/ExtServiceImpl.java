package org.jiucheng.magpiebridge.manager.service.impl;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.ioc.annotation.Service;
import org.jiucheng.plugin.db.BaseServiceImpl;
import org.jiucheng.plugin.db.IBaseDao;
import org.jiucheng.plugin.db.IBaseService;

@Service("baseService")
public class ExtServiceImpl extends BaseServiceImpl implements IBaseService {

    @Inject
    private IBaseDao baseDao;
    
    @Override
    public IBaseDao getBaseDao() {
        return baseDao;
    }
}
