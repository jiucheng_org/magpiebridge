package org.jiucheng.magpiebridge.manager.service.impl;

import java.util.List;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.ioc.annotation.Service;
import org.jiucheng.magpiebridge.manager.entity.ServerResource;
import org.jiucheng.magpiebridge.manager.service.IServerResourceService;
import org.jiucheng.orm.Sql;
import org.jiucheng.plugin.db.IBaseService;

@Service("serverResourceService")
public class ServerResourceServiceImpl implements IServerResourceService {
    @Inject
    private IBaseService baseService;
    
    public ServerResource lastServerResource(Long serverId, String serverProtocol) {
        if (serverId == null)
            return null;
        if (!("TCP".equalsIgnoreCase(serverProtocol) || "HTTP".equalsIgnoreCase(serverProtocol)))
            return null;
        Sql sh = new Sql();
        sh.append("SELECT * FROM server_resource WHERE server_id = ? AND server_protocol = ? ORDER BY id ASC limit 1");
        sh.insertValue(serverId);
        sh.insertValue(serverProtocol);
        List<ServerResource> list = baseService.getBaseDao().listBySQL(ServerResource.class, sh);
        if (list != null && !list.isEmpty())
            return list.get(0);
        return null;
    }
}
