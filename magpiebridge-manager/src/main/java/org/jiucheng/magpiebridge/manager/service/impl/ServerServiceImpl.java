package org.jiucheng.magpiebridge.manager.service.impl;

import java.util.List;

import org.jiucheng.aop.Aop;
import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.ioc.annotation.Service;
import org.jiucheng.magpiebridge.manager.entity.Client;
import org.jiucheng.magpiebridge.manager.entity.ClientMapping;
import org.jiucheng.magpiebridge.manager.entity.Server;
import org.jiucheng.magpiebridge.manager.entity.ServerResource;
import org.jiucheng.magpiebridge.manager.entity.ServerResourceRule;
import org.jiucheng.magpiebridge.manager.service.IServerService;
import org.jiucheng.orm.interceptor.Tx;
import org.jiucheng.plugin.db.IBaseService;

@Service("serverService")
public class ServerServiceImpl implements IServerService {
    
    @Inject
    private IBaseService baseService;

    @Aop(Tx.class)
    public void deletetServers(List<Long> ids) {
        if (ids != null && !ids.isEmpty()) {
            for (Long id : ids) {
                if (id != null) {
                    Server server = new Server();
                    server.setId(id);
                    baseService.delete(server);
                    
                    Client client = new Client();
                    client.setServerId(id);
                    baseService.delete(client);
                    
                    ClientMapping clientMapping = new ClientMapping();
                    clientMapping.setServerId(id);
                    baseService.delete(clientMapping);
                    
                    ServerResourceRule serverResourceRule = new ServerResourceRule();
                    serverResourceRule.setServerId(id);
                    baseService.delete(serverResourceRule);
                    
                    ServerResource serverResource = new ServerResource();
                    serverResource.setServerId(id);
                    baseService.delete(serverResource);
                }
            }
        }
    }
}
