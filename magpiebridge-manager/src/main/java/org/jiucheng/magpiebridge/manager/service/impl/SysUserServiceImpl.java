package org.jiucheng.magpiebridge.manager.service.impl;

import java.util.List;

import org.jiucheng.aop.Aop;
import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.ioc.annotation.Service;
import org.jiucheng.magpiebridge.manager.dto.SysUserDto;
import org.jiucheng.magpiebridge.manager.entity.SysUser;
import org.jiucheng.magpiebridge.manager.entity.SysUserServer;
import org.jiucheng.magpiebridge.manager.service.ISysUserService;
import org.jiucheng.magpiebridge.manager.util.PagePrevNext;
import org.jiucheng.orm.Sql;
import org.jiucheng.orm.interceptor.Tx;
import org.jiucheng.plugin.db.IBaseService;
import org.jiucheng.util.StringUtil;

@Service("sysUserService")
public class SysUserServiceImpl implements ISysUserService {
	private int pageCount = 15;
    @Inject
    private IBaseService baseService;
    
	@Override
	public PagePrevNext<SysUserDto> page(int currentPage, String email) {
		PagePrevNext<SysUserDto> page = new PagePrevNext<SysUserDto>();
		if (StringUtil.isNotBlank(email)) {
	        Sql sh = new Sql();
	        sh.append("SELECT a.*,IFNULL(b.port_count_limit,-1) port_count_limit,IFNULL(b.sub_count_limit,-1) sub_count_limit FROM sys_user a LEFT JOIN sys_user_server b ON a.id=b.user_id AND b.server_id=0 WHERE a.email = ? ");
	        sh.insertValue(email);
	        List<SysUserDto> list = baseService.getBaseDao().listBySQL(SysUserDto.class, sh);
	        page.setList(list);
	        return page;
		}
        Sql sh = new Sql();
        sh.append("SELECT a.*,IFNULL(b.port_count_limit,-1) port_count_limit,IFNULL(b.sub_count_limit,-1) sub_count_limit FROM sys_user a LEFT JOIN sys_user_server b ON a.id=b.user_id AND b.server_id=0 WHERE 1=1 ORDER BY id LIMIT ?, ?");
        sh.insertValue((currentPage - 1) * pageCount);
        sh.insertValue(pageCount);
        List<SysUserDto> list = baseService.getBaseDao().listBySQL(SysUserDto.class, sh);
        page.setList(list);
        if (currentPage > 1) {
        	page.setPaged("T");
            page.setPrev(currentPage - 1);
        }
        if (list != null && list.size() == pageCount) {
        	sh.clearSql();
        	sh.clearValues();
            sh.append("SELECT id FROM sys_user WHERE 1=1 ORDER BY id LIMIT ?, ?");
            sh.insertValue(currentPage * pageCount);
            sh.insertValue(1);
            list = baseService.getBaseDao().listBySQL(SysUserDto.class, sh);
            if (list != null && !list.isEmpty()) {
            	page.setPaged("T");
            	page.setNext(currentPage + 1);
            }
        }
        return page;
	}
	
	@Aop(Tx.class)
	@Override
	public void updateSysUser(SysUser sysUser, SysUserServer sysUserServer) {
		if (sysUser != null) {
			baseService.update(sysUser);
		}
		if (sysUserServer != null) {
			baseService.update(sysUserServer);
		}
	}
	
	@Aop(Tx.class)
	@Override
	public Long saveSysUser(SysUser sysUser, SysUserServer sysUserServer) {
		Long id = (Long) baseService.save(sysUser);
		sysUserServer.setUserId(id);
		baseService.save(sysUserServer);
		return id;
	}
	
	@Override
	public SysUserServer getSysUserServer(Long userId, Long serverId) {
		if (userId != null && serverId != null) {
			SysUserServer sysUserServer = new SysUserServer();
			sysUserServer.setUserId(userId);
			sysUserServer.setServerId(serverId);
			List<SysUserServer> list = baseService.list(sysUserServer);
			if (list != null && !list.isEmpty()) {
				return list.get(0);
			}
		}
		return null;
	}
}
