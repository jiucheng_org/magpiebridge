package org.jiucheng.magpiebridge.manager.util;

import feign.Body;
import feign.RequestLine;

public interface FeignRequest {
    @RequestLine("GET")
    public String get();
    
    @RequestLine("POST")
    @Body("{body}")
    public String post(String body);
}
