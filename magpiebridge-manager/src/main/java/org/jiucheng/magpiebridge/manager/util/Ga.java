package org.jiucheng.magpiebridge.manager.util;

import java.util.Map;
import java.util.Objects;

import org.jiucheng.util.StringUtil;

public class Ga {
	
	public static String selected(Object selected, Object value) {
		if (selected != null && value != null
				&& Objects.toString(selected).equals(Objects.toString(value)))
			return "selected=\"selected\"";
		return "";
	}
	
    public static String active(Map<String, Object> context, String exp) {
        if (exp.equals(context.get("uri"))) {
            return " active";
        }
        return StringUtil.EMPTY;
    }
}
