package org.jiucheng.magpiebridge.manager.util;

import java.util.List;

public class PagePrevNext<T> {

	private List<T> list;
	private String paged;
	private Integer prev;
	private Integer next;
	
	public String getPaged() {
		return paged;
	}
	
	public void setPaged(String paged) {
		this.paged = paged;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public Integer getPrev() {
		return prev;
	}

	public void setPrev(Integer prev) {
		this.prev = prev;
	}

	public Integer getNext() {
		return next;
	}

	public void setNext(Integer next) {
		this.next = next;
	}
}
