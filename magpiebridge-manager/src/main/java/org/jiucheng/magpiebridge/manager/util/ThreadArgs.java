package org.jiucheng.magpiebridge.manager.util;

public class ThreadArgs {
    
    private static final ThreadLocal<Long> GID = new ThreadLocal<Long>();
    private static final ThreadLocal<String> GEMAIL = new ThreadLocal<String>();
    private static final ThreadLocal<String> ISSUPER = new ThreadLocal<String>();
    
    public static void setGid(Long gid) {
        GID.set(gid);
    }
    
    public static Long getGid() {
        return GID.get();
    }
    
    public static void setGemail(String gemail) {
        GEMAIL.set(gemail);
    }
    
    public static String getGemail() {
        return GEMAIL.get();
    }
    
    public static void setIsSuper(String isSuper) {
        ISSUPER.set(isSuper);
    }
    
    public static String getIsSuper() {
        return ISSUPER.get();
    }
}
