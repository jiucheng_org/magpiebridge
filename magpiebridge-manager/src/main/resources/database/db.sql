CREATE TABLE IF NOT EXISTS `sys_user`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `is_super` VARCHAR(2) NOT NULL DEFAULT 'F',
    `inactive` VARCHAR(2) NOT NULL DEFAULT 'F',
    PRIMARY KEY(`id`),
    UNIQUE KEY `tb5_1`(`email`)
);
-- password=MD5('123456')
INSERT INTO `sys_user`(`email`, `password`,`is_super`,`inactive`)
SELECT 'admin@localhost', 'e10adc3949ba59abbe56e057f20f883e', 'T', 'F'
FROM `information_schema`.`tables`
WHERE `table_schema` = SCHEMA() AND `table_name` = 'sys_user' AND NOT EXISTS(SELECT 1 FROM `sys_user` WHERE `email` = 'admin@localhost');

INSERT INTO `sys_user`(`email`, `password`,`is_super`,`inactive`)
SELECT 'guest@localhost', 'e10adc3949ba59abbe56e057f20f883e', 'F', 'F'
FROM `information_schema`.`tables`
WHERE `table_schema` = SCHEMA() AND `table_name` = 'sys_user' AND NOT EXISTS(SELECT 1 FROM `sys_user` WHERE `email` = 'guest@localhost');

CREATE TABLE IF NOT EXISTS `server`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `token` VARCHAR(255) NOT NULL,
    `ip` VARCHAR(255) NOT NULL,
    `port` BIGINT(16) NOT NULL,
    `protocol_type` BIGINT(16) NOT NULL DEFAULT 0,
    `domain` VARCHAR(255) NOT NULL,
    `black_ports` VARCHAR(65535) NULL,
    `black_subs` VARCHAR(65535) NULL,
    PRIMARY KEY(`id`),
    UNIQUE KEY `tb1_1`(`token`)
);
INSERT INTO `server`(`token`, `name`, `ip`, `port`, `protocol_type`, `domain`, `black_ports`, `black_subs`)
SELECT 'd89f3a35931c386956c1a402a8e09941', '本地/测试/环境', '127.0.0.1', 9799, 0, 'jiucheng.org', '0-1023', 'www,wwww,wwwww,3g,wap,m,blog'
FROM `information_schema`.`tables`
WHERE `table_schema` = SCHEMA() AND `table_name` = 'server' AND NOT EXISTS(SELECT 1 FROM `server` WHERE `token` = 'd89f3a35931c386956c1a402a8e09941');

CREATE TABLE IF NOT EXISTS `sys_user_server`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT(16) NOT NULL,
    `server_id` BIGINT(16) NOT NULL,
    `port_count_limit` BIGINT(16) NOT NULL DEFAULT -1,
    `sub_count_limit` BIGINT(16) NOT NULL DEFAULT -1,
    `port_count` BIGINT(16) NOT NULL DEFAULT 0,
    `sub_count` BIGINT(16) NOT NULL DEFAULT 0,
    `ver` BIGINT(11) NOT NULL DEFAULT 0,
    PRIMARY KEY(`id`),
    UNIQUE KEY `tb8_1`(`user_id`,`server_id`)
);

INSERT INTO `sys_user_server`(`server_id`,`user_id`,`port_count`,`sub_count`)
SELECT 0, b.id,2,1
FROM sys_user b
WHERE b.email ='admin@localhost'
    AND NOT EXISTS (SELECT 1 FROM sys_user_server c where c.server_id = 0 AND c.user_id=b.id);

INSERT INTO `sys_user_server`(`server_id`,`user_id`,`port_count`,`sub_count`)
SELECT a.id, b.id,2,1
FROM server a, sys_user b
WHERE a.token = 'd89f3a35931c386956c1a402a8e09941' AND b.email ='admin@localhost'
    AND NOT EXISTS (SELECT 1 FROM sys_user_server c where c.server_id = a.id AND c.user_id=b.id);

INSERT INTO `sys_user_server`(`server_id`,`user_id`,`port_count_limit`,`sub_count_limit`)
SELECT 0, b.id,2,2
FROM sys_user b
WHERE b.email ='guest@localhost'
    AND NOT EXISTS (SELECT 1 FROM sys_user_server c where c.server_id = 0 AND c.user_id=b.id);

CREATE TABLE IF NOT EXISTS `client`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `token` VARCHAR(255) NOT NULL,
    `server_id` BIGINT(16) NOT NULL,
    `user_id` BIGINT(16) NOT NULL,
    PRIMARY KEY(`id`),
    KEY `tb3_1`(`server_id`, `token`),
    UNIQUE KEY `tb3_2`(`token`),
    KEY `tb3_2`(`user_id`)
);
INSERT INTO `client`(`name`,`token`,`server_id`,`user_id`)
SELECT '本地测试', 'd563d057c1bc45f781459faf8bf5b32b', a.id, b.id
FROM server a, sys_user b
WHERE a.token = 'd89f3a35931c386956c1a402a8e09941' AND b.email ='admin@localhost'
    AND NOT EXISTS (SELECT 1 FROM client c where c.server_id = a.id AND c.token = 'd563d057c1bc45f781459faf8bf5b32b');

CREATE TABLE IF NOT EXISTS `client_mapping`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `client_id` BIGINT(16) NOT NULL,
    `server_id` BIGINT(16) NOT NULL,
    `server_protocol` VARCHAR(255) NOT NULL,
    `server_ip_port` VARCHAR(255) NOT NULL,
    `local_ip_port` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`),
    UNIQUE KEY `tb4_1`(`server_id`, `server_protocol`, `server_ip_port`),
    KEY `tb4_2`(`client_id`)
);
INSERT INTO `client_mapping`(`client_id`,`server_id`, `server_protocol`, server_ip_port, `local_ip_port`)
SELECT b.id, b.server_id, 'TCP', '13389', '127.0.0.1:3389'
FROM server a, client b
WHERE a.id = b.server_id
    AND a.token = 'd89f3a35931c386956c1a402a8e09941' AND b.token = 'd563d057c1bc45f781459faf8bf5b32b'
    AND NOT EXISTS(SELECT * FROM client_mapping c where c.server_id = b.server_id AND c.client_id = b.id AND c.server_protocol = 'TCP' AND c.server_ip_port = '13389');

INSERT INTO `client_mapping`(`client_id`,`server_id`, `server_protocol`, server_ip_port, `local_ip_port`)
SELECT b.id, b.server_id, 'TCP', '13306', '127.0.0.1:3306'
FROM server a, client b
WHERE a.id = b.server_id
    AND a.token = 'd89f3a35931c386956c1a402a8e09941' AND b.token = 'd563d057c1bc45f781459faf8bf5b32b'
    AND NOT EXISTS(SELECT * FROM client_mapping c where c.server_id = b.server_id AND c.client_id = b.id AND c.server_protocol = 'TCP' AND c.server_ip_port = '13306');

INSERT INTO `client_mapping`(`client_id`,`server_id`, `server_protocol`, server_ip_port, `local_ip_port`)
SELECT b.id, b.server_id, 'HTTP', 'onlyfordev', '127.0.0.1:8080'
FROM server a, client b
WHERE a.id = b.server_id
    AND a.token = 'd89f3a35931c386956c1a402a8e09941' AND b.token = 'd563d057c1bc45f781459faf8bf5b32b'
    AND NOT EXISTS(SELECT * FROM client_mapping c where c.server_id = b.server_id AND c.client_id = b.id AND c.server_protocol = 'HTTP' AND c.server_ip_port = 'onlyfordev');

CREATE TABLE IF NOT EXISTS `server_resource_rule`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `server_id` BIGINT(16) NOT NULL,
    `current_port` BIGINT(16) NOT NULL,
    `current_sub` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`),
    UNIQUE KEY `tb6_1`(`server_id`)
);

CREATE TABLE IF NOT EXISTS `server_resource`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `server_id` BIGINT(16) NOT NULL,
    `server_protocol` VARCHAR(255) NOT NULL,
    `server_ip_port` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id`),
    UNIQUE KEY `tb7_1`(`server_id`, `server_protocol`, `server_ip_port`)
);

CREATE TABLE IF NOT EXISTS `oauth2_control`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `category` VARCHAR(255) NOT NULL,
    `content` VARCHAR(1024) NOT NULL,
    PRIMARY KEY(`id`),
    UNIQUE KEY `tb9_1`(`category`)
);