package org.magpiebridge.manager;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MultiThreadTest {
    public static void main(String[] args) {
        System.out.println("s=" + new Date());
        ExecutorService es = Executors.newFixedThreadPool(12);
        for (int i = 0; i < 1; i++) {
            final int idx = i;
            es.execute(new Runnable() {
                public void run() {
//                    try {
                        //TimeUnit.SECONDS.sleep(5);
                        System.out.println("i(" + idx + ")=" + new Date());
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                }
            });
        }
        es.shutdown();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        System.out.println("is=" + es.isShutdown() + "," + es.isTerminated());
        try {
            while (!es.awaitTermination(2, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
            es.shutdownNow();
        }
        System.out.println("e=" + new Date());
        System.out.println("over...");
    }
}
