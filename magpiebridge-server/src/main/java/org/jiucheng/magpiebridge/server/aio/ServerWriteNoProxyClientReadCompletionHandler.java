package org.jiucheng.magpiebridge.server.aio;

import java.nio.channels.CompletionHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author jiucheng
 *
 */
public class ServerWriteNoProxyClientReadCompletionHandler implements CompletionHandler<Integer, ServerAttachment> {
    
    private static final Logger LOGGER = Logger.getLogger(ServerWriteNoProxyClientReadCompletionHandler.class.getName());
    
    public void completed(Integer result, ServerAttachment attachment) {
        if (attachment.getWriteBuffer().hasRemaining()) {
            attachment.getServer().write(attachment.getWriteBuffer(), attachment, this);
            return;
        }
        attachment.writed.compareAndSet(true, false);
    }

    public void failed(Throwable exc, ServerAttachment attachment) {
        attachment.writed.compareAndSet(true, false);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "", exc);
        }
    }
}
