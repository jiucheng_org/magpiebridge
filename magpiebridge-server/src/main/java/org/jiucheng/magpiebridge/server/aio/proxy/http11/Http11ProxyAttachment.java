package org.jiucheng.magpiebridge.server.aio.proxy.http11;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * 
 * @author jiucheng
 *
 */
public class Http11ProxyAttachment {
    // Client连接
    AsynchronousSocketChannel server;
    // Client读入数据
    ByteBuffer readBuffer;
    
    public Http11ProxyAttachment(AsynchronousSocketChannel server) {
        this.server = server;
    }
    
    public ByteBuffer getReadBuffer() {
        return readBuffer;
    }
    
    public Http11ProxyAttachment setReadBuffer(ByteBuffer readBuffer) {
        this.readBuffer = readBuffer;
        return this;
    }
    
    public AsynchronousSocketChannel getServer() {
        return server;
    }

    public void close() {
        if (server != null) {
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
