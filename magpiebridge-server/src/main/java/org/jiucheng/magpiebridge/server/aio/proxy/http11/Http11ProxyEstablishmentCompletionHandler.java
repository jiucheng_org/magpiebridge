package org.jiucheng.magpiebridge.server.aio.proxy.http11;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * 
 * @author jiucheng
 *
 */
public class Http11ProxyEstablishmentCompletionHandler implements CompletionHandler<AsynchronousSocketChannel, AsynchronousServerSocketChannel>{

	public void completed(AsynchronousSocketChannel result, AsynchronousServerSocketChannel attachment) {
		attachment.accept(attachment, this);
		ByteBuffer readBuffer = ByteBuffer.allocate(2 * 1024);
		result.read(readBuffer, new Http11ProxyAttachment(result).setReadBuffer(readBuffer), new Http11ProxyReadCompletionHandler());
	}

	public void failed(Throwable exc, AsynchronousServerSocketChannel attachment) {
        System.out.println("only proxy server failed");
	}
}
